package com.example.Books.Model;

import javax.persistence.*;

@Entity
@Table(name = "bookdetails")
public class Book {
    @Id
    @GeneratedValue
    @Column(name = "id")
    Long id;
    @Column(name = "name")
    String name;
    @Column(name = "price")
    float price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
