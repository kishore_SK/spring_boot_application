package com.example.Books.Services;
import com.example.Books.Model.Book;
import com.example.Books.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServices{

    @Autowired
    BookRepository bookRepository;

    public List<Book> listAllBook() {
        return bookRepository.findAll();
    }

    public Optional<Book> listBookById(Long id) {
        return bookRepository.findById(id);
    }

    public void createBook(Book book) {
        bookRepository.save(book);
    }

    public void delete(Long id) {
        bookRepository.deleteById(id);

    }
}