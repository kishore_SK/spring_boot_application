package com.example.Books.Controller;

import com.example.Books.Model.Book;
import com.example.Books.Services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/books")
public class BooksController {

    @Autowired
    BookServices bookServices;

    @GetMapping
    public List<Book> getbook() {
        return bookServices.listAllBook();
    }


    @GetMapping("{id}")
    public Optional<Book> getBookById(@PathVariable("id") Long id) {
        return bookServices.listBookById(id);
    }


    @PostMapping
    public String createBook(@RequestBody Book book) {
        bookServices.createBook(book);
        return "book added successfully";
    }

    @DeleteMapping("{id}")
    public String deleteBook(@PathVariable("id") Long id) {
        bookServices.delete(id);
        return "Book is Deleted successfully";
    }


}
